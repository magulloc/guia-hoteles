$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({ interval: 3000 });

  $('#contacto').on('show.bs.modal', function (e) {
    console.log('Mostrar modal');
    $('.contactoBTN').prop('disabled', true);
  })
  $('#contacto').on('shown.bs.modal', function (e) { console.log('Mostrado modal'); })
  $('#contacto').on('hide.bs.modal', function (e) { console.log('Esconder modal'); })
  $('#contacto').on('hidden.bs.modal', function (e) {
    console.log('Escondido modal');
    $('.contactoBTN').prop('disabled', false);
  })
});
